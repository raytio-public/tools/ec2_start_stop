"""
 @license
 Copyright Raytio Ltd. All Rights Reserved.
 
 Use of this source code is governed by an MIT-style license that can be
 found in the LICENSE file at https://www.rayt.io/mit
"""

import boto3
import os

client = boto3.client('ssm')

API_ENV = "/" + \
    os.environ['API_ENV'] if os.environ['API_ENV'] is not None else "/dev"


def get_secret(key):
    resp = client.get_parameter(
        Name=key,
        WithDecryption=True
    )
    return resp['Parameter']['Value']


access_token = get_secret('supermanToken')

database_connection = get_secret('databaseConn')
