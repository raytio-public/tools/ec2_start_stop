"""
 @license
 Copyright Raytio Ltd. All Rights Reserved.
 
 Use of this source code is governed by an MIT-style license that can be
 found in the LICENSE file at https://www.rayt.io/mit
"""

import os
import ast
import boto3

API_ENV = "/" + \
    os.environ['API_ENV'] if os.environ['API_ENV'] is not None else "/dev"
client_ssm = boto3.client('ssm')


region_ssm = client_ssm.get_parameter(Name=API_ENV + "/aws_region")
region = region_ssm['Parameter']['Value']
print('region: ' + str(region))
instances_ssm = client_ssm.get_parameter(Name=API_ENV + "/ec2_instances")
print('instance: ' + str(instances_ssm))
instances_str = instances_ssm['Parameter']['Value']
print('instance: ' + str(instances_str))
instances = ast.literal_eval(instances_str)
print('instance: ' + str(instances))


def lambda_handler(event, context):
    ec2 = boto3.client('ec2', region_name=region)
    ec2.stop_instances(InstanceIds=instances)
    print('stopped instances: ' + str(instances))
